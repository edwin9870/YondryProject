package com.itla.proyectolibreria.entidad;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Edwin Ramirez Ventur
 */
@Entity
@Table(name = "book")
@NamedQueries({@NamedQuery(name = "Book.desplegarlibro", query = "SELECT a FROM Book a")})
@DiscriminatorValue("B")
public class Book extends Catalogitem {
    @Column(name = "autor")
    private String autor;

    @Column(name = "number_of_page")
    private int numberOfPage;

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getNumberOfPage() {
        return numberOfPage;
    }

    public void setNumberOfPage(int numberOfPage) {
        this.numberOfPage = numberOfPage;
    }

    @Override
    public String toString() {
        return "Book{" + "autor=" + autor + ", numberOfPage=" + numberOfPage + '}';
    }
}
