package com.itla.proyectolibreria.entidad;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Edwin Ramirez Ventur
 */
@Entity
@Table(name = "recording")
@NamedQueries({})
@DiscriminatorValue("R")
public class Recording extends Catalogitem {

    @Column(name = "performer")
    private String performer;

    @Column(name = "format_id")
    private int formatId;

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public int getFormatId() {
        return formatId;
    }

    public void setFormatId(int formatId) {
        this.formatId = formatId;
    }

    @Override
    public String toString() {
        return "Recording{" + "performer=" + performer + ", formatId=" + formatId + '}';
    }   
}
