/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itla.proyectolibreria.main;

import com.itla.proyectolibreria.entidad.Book;
import com.itla.proyectolibreria.entidad.Catalogitem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Edwin Ramirez Ventur
 */
public class InsertarArticulo {

    public static void main(String args[]) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("libreria");
        EntityManager em = emf.createEntityManager();
        
        Book book = new Book();
        book.setAutor("Gabriel Garcia");
        book.setAvailable(true);
        book.setCode(001);
        book.setNumberOfPage(70);
        book.setTitle("La hojarasca");
        book.setYear(1995);
        
        em.getTransaction().begin();
        em.persist(book);
        em.getTransaction().commit();
        System.out.println("sus datos han sido guardados con exito");
    }
}
