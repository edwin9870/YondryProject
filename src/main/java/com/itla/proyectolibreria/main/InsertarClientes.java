/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itla.proyectolibreria.main;

import com.itla.proyectolibreria.entidad.User;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Edwin Ramirez Ventur
 */
public class InsertarClientes {
    public static void main(String[] args){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("libreria");
        EntityManager em = emf.createEntityManager();
        
        User user = new User();
        user.setName("Juan Ramon");
        user.setCode(005);
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        System.out.println("el usuario ha sido guardado correctamente");
    }
}
