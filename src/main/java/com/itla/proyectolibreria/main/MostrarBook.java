/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itla.proyectolibreria.main;

import com.itla.proyectolibreria.entidad.Book;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Edwin Ramirez Ventur
 */
public class MostrarBook {
    public static void main(String[] args){
    
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("libreria");
        EntityManager em = emf.createEntityManager();
        
        List <Book> resultlist = em.createNamedQuery("Book.desplegarlibro", Book.class).getResultList();
        
        for(List listar: resultlist){
            System.out.println();
        }
    }
}
