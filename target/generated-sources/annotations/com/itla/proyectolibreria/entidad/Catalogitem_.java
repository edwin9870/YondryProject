package com.itla.proyectolibreria.entidad;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.4.v20160829-rNA", date="2016-11-26T20:17:28")
@StaticMetamodel(Catalogitem.class)
public abstract class Catalogitem_ { 

    public static volatile SingularAttribute<Catalogitem, Integer> code;
    public static volatile SingularAttribute<Catalogitem, Integer> year;
    public static volatile SingularAttribute<Catalogitem, Boolean> available;
    public static volatile SingularAttribute<Catalogitem, Integer> id;
    public static volatile SingularAttribute<Catalogitem, String> title;
    public static volatile SingularAttribute<Catalogitem, String> type;

}